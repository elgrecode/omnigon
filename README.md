#### Each post record should display:
- [x] Post date (formatted as DD/MM/YYYY HH:MM) in user's timezone
- [x] Author name
- [x] Message body



#### Component should accept the following configuration options:
- [?] Destination HTML element container
- [x] Feed URL (clickable)
- [x] Number of posts to display
- [x] Update interval
- [x] Component should be implemented with React
- [x] While implementing, think about loading and rendering performance, memory usage and leaks.


The following feed can be used as an example: http://api.massrelevance.com/MassRelDemo/kindle.json. It supports both CORS and JSONP. It also provides some Web API: http://dev.massrelevance.com/docs/api/v1.0/stream/#ref-params-standard


Bonus points for
- [x] more advanced webpack/babel configurations,
- [ ] adding unit tests,
- [x] atomic component design and or
- [x] use of higher order components, and
- [x] UI/UX best practices.


#### Getting Started
Make sure you have yarn installed. Then, to get started:
`yarn install`
`yarn start`

A browser window should open up so you can poke around on `localhost:3000`
