// Browser Rendering
import React from 'react';
import ReactDOM from 'react-dom';

// Polyfills
import win from './js/polyfills/win';

// Components
import AppLayout from './js/components/AppLayout';
import SocialFeed from './js/components/SocialFeed';

// Css
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const appComponent = (
  <AppLayout>
    <SocialFeed feedUrl="//api.massrelevance.com/MassRelDemo/kindle.json" />
  </AppLayout>
);

ReactDOM.render(appComponent, win.document.getElementById('root'));
registerServiceWorker();
