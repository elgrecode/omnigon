/* global window */
/* NOTE for Mark: I use this as a way to have a standard entry point when referencing the window. */
/* This is out of consideration for server side rendering where node has no *window* concept */
/* When we *rarely* :) reference the window in React, this will be our stubbed object */
/* This isn't really relevant for this test purposes, but I do this out of habit */

const win = typeof window === 'object'
  ? window
  : {
    isMockWindow: true,
    document: {},
    setInterval: () => {},
    addEventListener: () => {},
    removeEventListener: () => {},
  };

export default win;
