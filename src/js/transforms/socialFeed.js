export function transformFeedItems(feedItems) {
  return feedItems.map(item => ({
    id: item.id,
    postDate: item.created_at,
    authorName: item.user.name,
    authorTimezone: item.user.time_zone,
    authorUrl: item.user.url,
    entityId: item.entity_id,
    messageBody: item.text,
    profileImgUrl: item.user.profile_image_url,
  }));
}

export default {
  transformFeedItems,
};
