export function generatePlaceholders(count) {
  const arrPlaceholders = [];
  for (let i = 0; i < count; i += 1) {
    arrPlaceholders[i] = { id: i };
  }
  return arrPlaceholders;
}

export default {
  generatePlaceholders,
};
