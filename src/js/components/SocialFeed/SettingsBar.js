import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import autobind from 'react-autobind';
import classnames from 'classnames';

// Components
import Input from '../form/Input';

// Constants
import DEFAULT_FEED_PARAMS from '../../constants/FeedConstants';

// CSS
import '../../../css/components/SettingsBar.scss';

const MIN_INTERVAL = 20;
const MAX_LIMIT = 200;

/* eslint-disable react/prefer-stateless-function */
class SettingsBar extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      isOpen: false,
    };
  }

  handleOpenClick() {
    this.setState({
      isOpen: true,
    });
  }

  handleLimitChange({ value }) {
    if (!value) return;
    this.props.handleLimitChange({
      limit: parseInt(value, 10),
    });
  }

  handleIntervalChange({ value }) {
    if (!value || parseInt(value, 10) < MIN_INTERVAL) return;
    this.props.handleIntervalChange({
      interval: parseInt(value, 10),
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isHovering && !nextProps.isHovering) {
      this.setState({
        isOpen: false,
      });
    }
  }

  render() {
    const { isHovering } = this.props;
    const { isOpen } = this.state;

    return (
      <div className={classnames(
        'SettingsBar u-width--full grid-center-middle',
        { 'SettingsBar--open': isOpen && isHovering },
      )}
      >
        <div className="SettingsBar__contents grid-center-middle">
          <div className="SettingsBar__form">
            <form>
              <div className="SettingsBar__form-display-count">
                <label htmlFor="limit h3">How many posts to display at one time?</label>
                <Input
                  id="limit"
                  defaultValue={DEFAULT_FEED_PARAMS.limit}
                  type="number"
                  max={MAX_LIMIT}
                  onChange={this.handleLimitChange}
                />
              </div>

              <div className="SettingsBar__form-update-interval u-mt--normal">
                <label htmlFor="updateInterval h3">How often should the feed update?</label>
                <Input
                  id="updateInterval"
                  defaultValue={DEFAULT_FEED_PARAMS.interval}
                  type="number"
                  step="10"
                  min={MIN_INTERVAL}
                  onChange={this.handleIntervalChange}
                  onBlur={this.handleIntervalChange}
                /> <span className="h3">seconds</span>
              </div>
            </form>
          </div>
          <div className="SettingsBar__open-btn-wrapper grid-center-middle u-width--full">
            <h3>
              <span
                className="SettingsBar__open-btn"
                tabIndex="-1"
                onClick={this.handleOpenClick}
                onKeyDown={this.handleOpenClick}
                role="button"
              >
              Settings
              </span>
            </h3>
          </div>
        </div>
      </div>
    );
  }
}

SettingsBar.propTypes = {
  // Decorator Props
  isHovering: PropTypes.bool,
  // Funcs
  handleLimitChange: PropTypes.func.isRequired,
  handleIntervalChange: PropTypes.func.isRequired,
};

SettingsBar.defaultProps = {
  isHovering: false,
};

export default SettingsBar;
