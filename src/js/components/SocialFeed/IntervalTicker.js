import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import autobind from 'react-autobind';

// Decorators
import Tick from '../decorators/Tick';

// CSS
import '../../../css/components/IntervalTicker.scss';

/* eslint-disable react/prefer-stateless-function */
class IntervalTicker extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }


  render() {
    const { secondTicker } = this.props;
    return (
      <div className="IntervalTicker grid-middle u-position--fixed">
        <span>Time until next update:&nbsp;
          <span className="u-text-decoration--underline">
            {secondTicker}
          </span>
          &nbsp;seconds
        </span>
      </div>
    );
  }
}

IntervalTicker.propTypes = {
  // Decorator Props
  secondTicker: PropTypes.number.isRequired,
};

export default Tick(1000)(IntervalTicker);
