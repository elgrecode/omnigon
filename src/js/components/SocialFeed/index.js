import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ReactHoverObserver from 'react-hover-observer';
import autobind from 'react-autobind';
import classnames from 'classnames';

// Services
import { getFeedData } from '../../services/FeedService';

// Components
import FeedItem from './FeedItem';
import SettingsBar from './SettingsBar';
import IntervalTicker from './IntervalTicker';

// Polyfills
import win from '../../polyfills/win';

// Transforms
import { transformFeedItems } from '../../transforms/socialFeed';

// Transforms
import { generatePlaceholders } from '../../utilities/FeedUtilities';

// Constants
import DEFAULT_FEED_PARAMS from '../../constants/FeedConstants';

// CSS
import '../../../css/components/SocialFeed.scss';

class SocialFeed extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      interval: DEFAULT_FEED_PARAMS.interval,
      intervalId: null,
      nextUpdateTimestamp: 0,
      limit: DEFAULT_FEED_PARAMS.limit,

      lastEntityId: null,
      lastColChange: 0,
      direction: 'right',
      col0Listings: generatePlaceholders(DEFAULT_FEED_PARAMS.limit),
      col1Listings: generatePlaceholders(DEFAULT_FEED_PARAMS.limit),
      col2Listings: generatePlaceholders(DEFAULT_FEED_PARAMS.limit),
    };
  }

  determineEntryStatePositionChange(entries) {
    // This is a three col approach which leads to interesting animation
    const { direction, lastColChange } = this.state;
    if (lastColChange === 0) {
      this.setState({
        lastColChange: 1,
        col1Listings: entries,
        direction: 'right',
      });
    }

    if (lastColChange === 1 && direction === 'right') {
      this.setState({
        lastColChange: 2,
        col2Listings: entries,
      });
    }

    if (lastColChange === 1 && direction === 'left') {
      this.setState({
        lastColChange: 0,
        col0Listings: entries,
      });
    }

    if (lastColChange === 2) {
      this.setState({
        lastColChange: 1,
        col1Listings: entries,
        direction: 'left',
      });
    }
  }

  fetchDataListings() {
    const { limit, lastEntityId } = this.state;
    const { feedUrl } = this.props;

    getFeedData({ limit, start_id: lastEntityId }, feedUrl)
      .then((res) => {
        const feedItems = transformFeedItems(res.body);
        const lastEntity = feedItems[feedItems.length - 1];
        this.setState({
          lastEntityId: lastEntity.entityId,
        });
        this.determineEntryStatePositionChange(feedItems);
      });
  }

  handleLimitChange({ limit }) {
    this.setState({ limit });
  }

  extendTargetUpdateTime(intervalInMilliseconds) {
    return Date.now() + intervalInMilliseconds;
  }


  /**
   * Handles an interval change by clearing and resetting a new interval
   * @param  {Number} interval
   */
  handleIntervalChange({ interval }) {
    const { intervalId } = this.state;
    if (intervalId) {
      win.clearInterval(intervalId);
      this.createNewFeedInterval(interval);
      this.setState({ interval });
    }
  }

  triggerIntervalFetch() {
    const { interval: intervalInSeconds } = this.state;
    const intervalInMilliseconds = parseInt(intervalInSeconds, 10) * 1000;
    this.fetchDataListings();
    this.setState({
      nextUpdateTimestamp: this.extendTargetUpdateTime(intervalInMilliseconds),
    });
  }

  createNewFeedInterval(intervalInSeconds) {
    const intervalInMilliseconds = parseInt(intervalInSeconds, 10) * 1000;

    // Setting Feed Update Interval
    // This starts an initial cycle that can only be interrupted by an settings change for interval
    const intervalId = win.setInterval(this.triggerIntervalFetch, intervalInMilliseconds);
    this.setState({
      intervalId,
      nextUpdateTimestamp: this.extendTargetUpdateTime(intervalInMilliseconds),
    });
  }

  componentDidMount() {
    this.fetchDataListings();
    this.createNewFeedInterval(this.state.interval);
  }

  render() {
    const {
      lastColChange,
      col0Listings,
      col1Listings,
      col2Listings,
      nextUpdateTimestamp,
    } = this.state;

    return (
      <div className="SocialFeed u-center">
        <IntervalTicker nextUpdateTimestamp={nextUpdateTimestamp} />
        <ReactHoverObserver hoverOffDelayInMs={300}>
          <SettingsBar
            nextUpdateTimestamp={nextUpdateTimestamp}
            handleLimitChange={this.handleLimitChange}
            handleIntervalChange={this.handleIntervalChange}
          />
        </ReactHoverObserver>

        <div className={classnames(
          'SocialFeed__tri-column',
          {
            'SocialFeed__tri-col-shift-0': lastColChange === 0,
            'SocialFeed__tri-col-shift-1': lastColChange === 1,
            'SocialFeed__tri-col-shift-2': lastColChange === 2,
          },
        )}
        >
          <div className="SocialFeed__col SocialFeed__column0">
            {col0Listings.map(e => (
              <FeedItem
                key={e.id}
                authorName={e.authorName}
                authorTimezone={e.authorTimezone}
                authorUrl={e.authorUrl}
                messageBody={e.messageBody}
                postDate={e.postDate}
                profileImgUrl={e.profileImgUrl}
              />
            ))}
          </div>
          <div className="SocialFeed__col SocialFeed__column1">
            {col1Listings.map(e => (
              <FeedItem
                key={e.id}
                authorName={e.authorName}
                authorTimezone={e.authorTimezone}
                authorUrl={e.authorUrl}
                messageBody={e.messageBody}
                postDate={e.postDate}
                profileImgUrl={e.profileImgUrl}
              />
            ))}
          </div>
          <div className="SocialFeed__col SocialFeed__column2">
            {col2Listings.map(e => (
              <FeedItem
                key={e.id}
                authorName={e.authorName}
                authorTimezone={e.authorTimezone}
                authorUrl={e.authorUrl}
                messageBody={e.messageBody}
                postDate={e.postDate}
                profileImgUrl={e.profileImgUrl}
              />
            ))}
          </div>
        </div>

      </div>
    );
  }
}

SocialFeed.propTypes = {
  feedUrl: PropTypes.string.isRequired,
};

export default SocialFeed;
