import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';

// Assets
import cardBackground from '../../../img/footer_lodyas.png';

// CSS
import '../../../css/components/FeedItem.scss';

/* eslint-disable react/prefer-stateless-function */
class FeedItem extends PureComponent {
  generateBackgroundImage() {
    return {
      backgroundImage: `url(${cardBackground})`,
    };
  }
  render() {
    const {
      authorName,
      authorTimezone,
      authorUrl,
      postDate,
      profileImgUrl,
      messageBody,
    } = this.props;
    const momentDatePosted = authorTimezone
      ? moment(postDate).tz(authorTimezone)
      : moment(postDate);

    return (
      <div
        className="FeedItem"
        data-background={cardBackground}
        style={this.generateBackgroundImage()}
      >
        <a href={authorUrl} target="__blank">
          <div className="FeedItem__contents grid-12 u-height--full">
            <div className="col-5 FeedItem__meta grid-center-middle u-height--full">
              <div className="u-width--full">
                { profileImgUrl
                  ? (
                    <img
                      className="FeedItem__profile-img u-display--block u-center"
                      src={profileImgUrl}
                      alt="user profile"
                    />
                  ) : null
                }
                <div className="FeedItem__meta-text u-text-align--center">
                  <h2 className="u-mt--normal">{authorName}</h2>
                </div>
              </div>
            </div>
            <div className="col-7 FeedItem__text grid-middle">
              <p>{messageBody}</p>
            </div>
          </div>
          <div className="u-width--full u-text-align--center">
            {postDate ?
              (
                <p className="FeedItem__date">
                  {momentDatePosted.format('MMMM Do YYYY, h:mm:ss')}
                </p>
              ) : null
            }
          </div>
        </a>
      </div>
    );
  }
}

FeedItem.propTypes = {
  authorName: PropTypes.string,
  authorTimezone: PropTypes.string,
  authorUrl: PropTypes.string,
  postDate: PropTypes.string, // timestamp
  profileImgUrl: PropTypes.string,
  messageBody: PropTypes.string,
};

FeedItem.defaultProps = {
  authorName: null,
  authorTimezone: null,
  authorUrl: '',
  postDate: null,
  profileImgUrl: '',
  messageBody: '',
};

export default FeedItem;
