import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import autobind from 'react-autobind';

// CSS
import '../../../css/components/Input.scss';

/* eslint-disable react/prefer-stateless-function */
class Input extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  handleBlur(e) {
    const { onBlur, id } = this.props;
    onBlur({ id, value: e.target.value });
  }

  handleChange(e) {
    const { onChange, id } = this.props;
    onChange({ id, value: e.target.value });
  }

  render() {
    const {
      id,
      defaultValue,
      min,
      step,
      type,
    } = this.props;

    return (
      <input
        id={id}
        defaultValue={defaultValue}
        className="Input"
        type={type}
        min={min}
        step={step}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
      />
    );
  }
}

Input.propTypes = {
  id: PropTypes.string.isRequired,
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  min: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  step: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  type: PropTypes.string,
  // Redux Funcs
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
};

Input.defaultProps = {
  defaultValue: null,
  min: null,
  step: null,
  type: null,
  onBlur: () => {},
  onChange: () => {},
};

export default Input;
