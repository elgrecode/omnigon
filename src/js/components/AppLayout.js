import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// CSS
import '../../css/reset.scss';
import '../../css/gridlex.scss';
import '../../css/typography.scss';
import '../../css/helpers.scss';
import '../../css/components/AppLayout.scss';


class AppLayout extends PureComponent {
  render() {
    return (
      <div className="AppLayout">
        {this.props.children}
      </div>
    );
  }
}

AppLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppLayout;
