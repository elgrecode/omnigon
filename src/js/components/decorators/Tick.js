import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Polyfills
import win from '../../polyfills/win';

export default function TickConfigThunk(msTick) {
  return function decorateClass(DecoratedComponent) {
    class Tick extends Component {
      constructor(...args) {
        super(...args);
        this.state = {
          secondTicker: 0,
        };
      }

      calculateCountdown() {
        const { nextUpdateTimestamp } = this.props;
        return Math.floor((nextUpdateTimestamp - Date.now()) / 1000);
      }

      setTickInterval() {
        win.setInterval(() => {
          this.setState({
            secondTicker: this.calculateCountdown(),
          });
        }, msTick);
      }

      componentDidMount() {
        this.setTickInterval();
      }

      render() {
        return (
          <DecoratedComponent
            {...this.props}
            {...this.state}
          />
        );
      }
    }

    Tick.propTypes = {
      nextUpdateTimestamp: PropTypes.number.isRequired,
    };

    return Tick;
  };
}
