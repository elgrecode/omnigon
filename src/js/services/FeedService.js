import request from 'superagent';

export function getFeedData(feedData, feedUrl) {
  return request
    .get(feedUrl)
    .set('Accept', 'application/json')
    .query(feedData);
}

export default {
  getFeedData,
};
